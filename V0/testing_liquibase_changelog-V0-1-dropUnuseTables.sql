--liquibase formatted sql
--useSSL=false

--changeset  Monu:LPST-1213    --context:rpm-1231    --labels:DB_REFACTORING_OR_NOV_RELEASE  --runOnChange:true   endDelimiter:#
   
--comment: Remove_Listing_Unuse_Tables  



   DROP procedure IF EXISTS `Removal_Unuse_tables`;

#
CREATE PROCEDURE  Removal_Unuse_tables(STATUS_RMV_RLB varchar(10))
BEGIN
DECLARE STATUS varchar (10);
DECLARE TBL_SCHEMA,TBL_NAME varchar (250);
DECLARE done INT DEFAULT FALSE;
DECLARE done1 INT DEFAULT FALSE;
DECLARE T_SCHEDULE_START DATE; DECLARE T_STATUS VARCHAR(250);

DECLARE cur CURSOR FOR select TABLE_SCHEMA, TABLE_NAME  from  UNUSE_TABLE_LIST_V0_0;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
SET STATUS =STATUS_RMV_RLB;

OPEN cur;

UNUSE_TBL_DROP_LOOP: LOOP

FETCH cur INTO  TBL_SCHEMA,TBL_NAME;


IF done THEN
LEAVE UNUSE_TBL_DROP_LOOP;
END IF;
  if STATUS ='RMV' then
SET @A =CONCAT("DROP TABLE IF EXISTS ", TBL_SCHEMA,".",TBL_NAME,";  ");
end if;
  if STATUS ='RLB' then
SET @A =CONCAT("CREATE TABLE IF NOT EXISTS ", TBL_SCHEMA,".",TBL_NAME," LIKE Neighbourly_Base25thSep_blankCopy.",TBL_NAME," ;  ");
end if;

PREPARE QUERY FROM @A;
execute  QUERY;
DEALLOCATE PREPARE QUERY;


END LOOP;  
CLOSE cur;  


END 
#



call Removal_Unuse_tables("RMV");


--Rollback call Removal_Unuse_tables("RLB");
--Rollback DROP procedure IF EXISTS `Removal_Unuse_tables`;













