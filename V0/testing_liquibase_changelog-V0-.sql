--liquibase formatted sql
--useSSL=false

--changeset  Monu:LPST-1212   --context:rpm-1231  --labels:DB_REFACTORING_OR_NOV_RELEASE  --runOnChange:true   endDelimiter:#
--comment: Listing_Unuse_Tables  
 








DROP procedure IF EXISTS `MAKE_UNUSETABLE_LIST`;

#
CREATE PROCEDURE  `MAKE_UNUSETABLE_LIST`()
BEGIN

DECLARE TBL_SCHEMA,TBL_NAME varchar (250);


  DECLARE  Refactoring_DB varchar (250);
select database() into Refactoring_DB;
create table 
if not exists UNUSE_TABLE_LIST_V0_0(id int(11) NOT NULL auto_increment,
										TABLE_SCHEMA varchar(250),
										TABLE_NAME varchar(250),
                                        PRIMARY KEY  (id));
insert into  UNUSE_TABLE_LIST_V0_0 (TABLE_SCHEMA ,TABLE_NAME)  SELECT TABLE_SCHEMA, TABLE_NAME FROM
    information_schema.tables
WHERE
    TABLE_SCHEMA = Refactoring_DB
        AND TABLE_NAME LIKE '%BACKUP%' 
UNION SELECT 
    TABLE_SCHEMA, TABLE_NAME
FROM
    information_schema.tables
WHERE
    TABLE_SCHEMA = Refactoring_DB
        AND TABLE_NAME LIKE '%BKP%' 
UNION SELECT 
    TABLE_SCHEMA, TABLE_NAME
FROM
    information_schema.tables
WHERE
    TABLE_SCHEMA = Refactoring_DB
        AND TABLE_NAME LIKE '%TEMP%' 
UNION SELECT 
    TABLE_SCHEMA, TABLE_NAME
FROM
    information_schema.tables
WHERE
    TABLE_SCHEMA = Refactoring_DB
        AND TABLE_NAME LIKE '%PORTING%' 
UNION SELECT
    TABLE_SCHEMA, TABLE_NAME
FROM
    information_schema.tables
WHERE
    TABLE_SCHEMA = Refactoring_DB
        AND TABLE_NAME LIKE '%PORT%'  and TABLE_NAME NOT LIKE '%REPORT%'  and TABLE_NAME NOT LIKE '%PORTAL%'  and TABLE_NAME NOT
 LIKE '%IMPORT%'  and TABLE_NAME NOT LIKE '%OPPORTUNITY%' and TABLE_NAME NOT LIKE '%SUPPORT%'


UNION SELECT 
    TABLE_SCHEMA, TABLE_NAME
FROM
    information_schema.tables
WHERE
    TABLE_SCHEMA = Refactoring_DB
        AND TABLE_NAME LIKE '%backup%' 
UNION SELECT 
    TABLE_SCHEMA, TABLE_NAME
FROM
    information_schema.tables
WHERE
    TABLE_SCHEMA = Refactoring_DB
        AND TABLE_NAME LIKE '%bkp%' 
UNION SELECT 
    TABLE_SCHEMA, TABLE_NAME
FROM
    information_schema.tables
WHERE
    TABLE_SCHEMA = Refactoring_DB
        AND TABLE_NAME LIKE '%temp%' 
        

UNION SELECT 
    TABLE_SCHEMA, TABLE_NAME
FROM
    information_schema.tables
WHERE
    TABLE_SCHEMA = Refactoring_DB
        AND TABLE_NAME LIKE '%AireServ%' 
UNION SELECT 
    TABLE_SCHEMA, TABLE_NAME
FROM
    information_schema.tables
WHERE
    TABLE_SCHEMA = Refactoring_DB
        AND TABLE_NAME LIKE '%GlassDoctor%' 
UNION SELECT 
    TABLE_SCHEMA, TABLE_NAME
FROM
    information_schema.tables
WHERE
    TABLE_SCHEMA = Refactoring_DB
        AND TABLE_NAME LIKE '%GroundGuys%' 
UNION SELECT 
    TABLE_SCHEMA, TABLE_NAME
FROM
    information_schema.tables
WHERE
    TABLE_SCHEMA = Refactoring_DB
        AND TABLE_NAME LIKE '%MollyMaid%' 
UNION SELECT 
    TABLE_SCHEMA, TABLE_NAME
FROM
    information_schema.tables
WHERE
    TABLE_SCHEMA = Refactoring_DB
        AND TABLE_NAME LIKE '%TheDwyerGroup%' 
UNION SELECT 
    TABLE_SCHEMA, TABLE_NAME
FROM
    information_schema.tables
WHERE
    TABLE_SCHEMA = Refactoring_DB
        AND TABLE_NAME LIKE '%MrAppliance%'
UNION SELECT 
    TABLE_SCHEMA, TABLE_NAME
FROM
    information_schema.tables
WHERE
    TABLE_SCHEMA = Refactoring_DB
        AND TABLE_NAME LIKE '%MrElectric%'
UNION SELECT 
    TABLE_SCHEMA, TABLE_NAME
FROM
    information_schema.tables
WHERE
    TABLE_SCHEMA = Refactoring_DB
        AND TABLE_NAME LIKE '%MrHandy%'
UNION SELECT 
    TABLE_SCHEMA, TABLE_NAME
FROM
    information_schema.tables
WHERE
    TABLE_SCHEMA = Refactoring_DB
        AND TABLE_NAME LIKE '%MrRooter%'
UNION SELECT 
    TABLE_SCHEMA, TABLE_NAME
FROM
    information_schema.tables
WHERE
    TABLE_SCHEMA = Refactoring_DB
        AND TABLE_NAME LIKE '%Rainbow%'
UNION
SELECT 
    TABLE_SCHEMA, TABLE_NAME
FROM
    information_schema.tables
WHERE
    TABLE_SCHEMA = Refactoring_DB
        AND TABLE_NAME LIKE '%2019%'

UNION
SELECT 
    TABLE_SCHEMA, TABLE_NAME
FROM
    information_schema.tables
WHERE
    TABLE_SCHEMA = Refactoring_DB
        AND TABLE_NAME LIKE '%2018%'
UNION
SELECT 
    TABLE_SCHEMA, TABLE_NAME
FROM
    information_schema.tables
WHERE
    TABLE_SCHEMA = Refactoring_DB
        AND TABLE_NAME LIKE '%2017%'

UNION SELECT 
    TABLE_SCHEMA, TABLE_NAME
FROM
    information_schema.tables
WHERE
    TABLE_SCHEMA = Refactoring_DB
        AND TABLE_NAME LIKE '%RealPropertyManagement%'  
  order by (case when TABLE_NAME like '%bKP%' or TABLE_NAME like '%back%'   or TABLE_NAME like '%PORT%' or  TABLE_NAME like '%TEMP%' or  TABLE_NAME like '%tmp%' or TABLE_NAME like '%2019%' or TABLE_NAME like '%2017%' or TABLE_NAME like '%2018%' then 0 else 1 end)  asc, TABLE_NAME;



END 
#











  


call MAKE_UNUSETABLE_LIST();
#
create table if not exists FRAN_OL(id int,player varchar(200));
#
create table if not exists FRAN_OLYMPIC(id int,player varchar(200)); #
--rollback DROP procedure IF EXISTS `MAKE_UNUSETABLE_LIST`;
--rollback DROP table  if exists `UNUSE_TABLE_LIST_V0_0`;
--rollback drop table if exists  FRAN_OLYMPIC;
 





